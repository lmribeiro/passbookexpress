package pt.m4f.passbookexpress.services;

import android.support.v4.app.Fragment;

import pt.m4f.passbookexpress.app.TicketBackFragment;
import pt.m4f.passbookexpress.app.TicketFrontFragment;

import java.io.File;

/**
 * Created by luis on 02-06-2013.
 */

public class EventTicketPassStrategy implements IPassStrategy {

	private File dir;

	public EventTicketPassStrategy(File dir) {
		this.dir = dir;
	}
	
	@Override
	public Fragment getFrontFragment() {
		return TicketFrontFragment.newInstance(this.dir.getPath());		
	}

	@Override
	public Fragment getBackFragment() {
		return TicketBackFragment.newInstance(this.dir.getPath());
	}

}
