package pt.m4f.passbookexpress.services;

import android.support.v4.app.Fragment;

/**
 * Created by luis on 02-06-2013.
 */

public interface IPassStrategy {
	Fragment getFrontFragment();
	Fragment getBackFragment();
}
