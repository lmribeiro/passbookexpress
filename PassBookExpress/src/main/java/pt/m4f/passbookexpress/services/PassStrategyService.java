package pt.m4f.passbookexpress.services;

import pt.m4f.passbookexpress.utils.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;

/**
 * Created by luis on 02-06-2013.
 */

public class PassStrategyService {
	public IPassStrategy getStrategy(File dir) throws IOException, ParseException {
		String json = new FileReader().readTextFile(new File(dir, "pass.json"));
		JSONParser parser = new JSONParser();
		JSONObject root = (JSONObject) parser.parse(json);
		
		if (root.containsKey("eventTicket")) {
			return new EventTicketPassStrategy(dir);
		}
		
		return null;
	}
}
